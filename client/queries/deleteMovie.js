import gql from "graphql-tag";

//requete graphql via tag
const mutation = gql`
mutation DeleteMovie($id: ID){
    deleteMovie(id: $id){
        id,
        title
    }
}`;

export default mutation;