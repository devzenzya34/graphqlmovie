import gql from "graphql-tag";

//requete graphql via tag
const query = gql`
{
    movies{
        id,
        title
    }
}`;

export default query;