import gql from "graphql-tag";

//requete graphql via tag
const mutation = gql`
mutation LikeReview($id: ID!){
    likeReview(id: $id){
        id,
        likes
    }
}`;

export default mutation;