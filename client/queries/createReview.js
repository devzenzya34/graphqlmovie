import gql from "graphql-tag";

//requete graphql via tag
const mutation = gql`
mutation CreateReview($content: String, $movieId: ID!){
    addReviewToMovie(content: $content, movieId: $movieId){
        id,
        title,
        reviews {
            id,
            content,
            likes
        }
    }
}`;

export default mutation;