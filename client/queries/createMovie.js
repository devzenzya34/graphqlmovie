import gql from "graphql-tag";

//requete graphql via tag
const mutation = gql`
mutation AddMovie($title: String){
    addMovie(title: $title){
        id,
        title
    }
}`;

export default mutation;