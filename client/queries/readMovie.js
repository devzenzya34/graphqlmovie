import gql from "graphql-tag";

//requete graphql via tag
const query = gql`
query ReadMovie($id: ID!){
    movie(id: $id){
        id,
        title,
        reviews{
            id,
            content,
            likes
        }
    }
}`;

export default query;