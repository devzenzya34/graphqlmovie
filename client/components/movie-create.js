import React, { Component } from 'react'
import { graphql } from "react-apollo";
import createMovieMutation from "../queries/createMovie";
import readMovieQuery from "../queries/readMovies";
import { hashHistory } from "react-router";

class MovieCreate extends Component {
    constructor(props){
        super(props);
        this.state = { terms : ""};
    }
    render() {
        return (
            <div>
                <h1>Movie Create</h1>
                <form className="input-field col s6">
                    <input type="text"
                    className="validate"
                    onChange= {e => this.setState({terms: e.target.value})}
                    onKeyPress={this.handleSubmitForm.bind(this)}
                    />
                    <label className="active"> Titre du film </label>
                </form>                
            </div>
        )
    }
    // Integration dela mutation pour activer la submission quand on appui sur une touche
    handleSubmitForm(e){
        if(e.key ==="Enter"){
            //evite le rechargement de la page à la soumission du formulaire
            e.preventDefault();
            //appel de la requête mutation (terms=champ)
            this.props.mutate({
                variables: {
                    title: this.state.terms
                },
                // recharge le cache avt le retour à la page d'acceuil pour avoir toute les nouvelles entrées.
                refetchQueries: [{query: readMovieQuery}]
            }).then( () => {
                hashHistory.push("/movies");
            })
        }
    }
}

export default graphql(createMovieMutation)(MovieCreate);