import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import { Link } from "react-router";
import readMoviesQuery from "../queries/readMovies";
import deleteMovieMutation from "../queries/deleteMovie";

class MovieList extends Component {
    render() {
        console.log(this.props)
        return (
            <div>
                <h1>Liste de Films</h1>
                <ul className="collection">
                    {this.renderMovies()}
                </ul>
                {/* Boutton ajouter movie avec le component link de react-router */}
                <Link to="/movies/create" className="btn-floating btn-large waves-effect waves-light blue right">
                    <i className="material-icons">add</i>
                </Link>
            </div>

        )
    }
    //fonction pour afficher les movies
    renderMovies() {
        if (!this.props.readMoviesQuery.loading) {
            return this.props.readMoviesQuery.movies.map((movie) => {
                return (<li className="collection-item" key={movie.id}>
                    <i className="material-icons secondary-content delete_button" 
                        onClick= {() => this.onDeleteMovie(movie.id)}>delete</i>
                    <Link to={`/movie/${movie.id}`}>{movie.title}</Link>
                </li>);

            })
        }
        else {
            return "Chargement des données ...."
        }

    }

    onDeleteMovie(id){
        this.props.deleteMovieMutation({
            variables: {
                id: id
            }
        }).then ( () => {
            this.props.readMoviesQuery.refetch();
        })
    }
}

// //requete graphql via tag
// const query = gql`
// {
//     movies{
//         id,
//         title
//     }
// }`;


//on lance la requete tag dans le export default
export default compose(
    graphql(readMoviesQuery, {
        name:"readMoviesQuery"
    }),
    graphql(deleteMovieMutation, {
        name:"deleteMovieMutation"
    })
)(MovieList);
