import "./style/style.css";
import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from "apollo-client";
import { ApolloProvider } from "react-apollo";
import MovieList from "./components/movie-list";
//Mis en place de la navigation
import MovieCreate from "./components/movie-create";
import MovieDetail from "./components/movie-detail";
import { Router, Route, hashHistory, IndexRedirect } from "react-router";

const client = new ApolloClient({
    //remplace le refetch lorsque on a besoin de l'id pour la mis à jour de l'élément sur la meme page
    dataIdFromObject : o => o.id
});

const Root = () => {
    return (
        <ApolloProvider client={client}>
            <Router history={hashHistory}>
                {/* Mis en place des routes */}
                <Route path="/">
                    <IndexRedirect to="/movies" />
                    <Route path="/movies" component={MovieList} />
                    <Route path="/movies/create" component={MovieCreate} />
                    <Route path="movie/:id" component={MovieDetail} />

                </Route>
            </Router>
        </ApolloProvider>
    )
};

ReactDOM.render(
    <Root />,
    document.querySelector('#root')
);
